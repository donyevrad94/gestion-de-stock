<?php
       require_once 'db.php';

       $stmt = $pdo->query('SELECT * FROM produit');

       if(isset($_REQUEST['del']))
       {
       	$sup = intval($_GET['del']);

       	$sql = "DELETE FORM produit WHERE id=:_produit";
       	$query = $pdo->prepare($sql);
       	$query->bindPram(':id_produit', $sup , PDO::PARAM_STR);
       	$query->execute();
       	    echo "<script> window.location.href='produit.php'</script>";
       }


?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<?php require_once 'navbar.php'; ?>
	      <div class="container">
	      	<table class="table table-striped">
				<thead>
					<th>Image</th>
					<th>Code Article</th>
					<th>Designation</th>
					<th>Quantite</th>
					<th>Etat en stock</th>
					<th>Action</th>
				</thead>
				<tbody>
					<?php
					while ($row = $stmt->fetch()) {

					 	?>

					 	<tr>
					 		<td><img src="./uploads/<?php echo $row-> image_produit; ?>" alt=" "  class="image_product "></td>
						 	<td><?php echo $row-> code_article; ?> </td>
	                        <td><?php echo $row-> nom_article; ?> </td>
	                        <td><?php echo $row-> quantite; ?> </td>
	                        <td > <span class="badge bg-success">en Stock</span> </td>
	                        <td>
	                        	<button class="btn btn-primary"><i class="fas fa-edit"></i></button>
	                        	<a href="produit.php?del=<?php echo $row->id;?>"><button class="btn btn-danger" onclick="return confirm ('Voulez vous vraiment suprimer')"><i class="fas fa-trash"></i></button></a>
	                        </td>
					 	</tr> 
					<?php } ?>

			    </tbody>
			</table>
	      </div>


</body>
</html>